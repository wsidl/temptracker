# coding=utf-8
import collections
from memory_profiler import profile

__author__ = 'Will Siddall <will.siddall@gmail.com>'
__version__ = '0.0.1'


class TempTracker(object):
    """
    Temperature Tracker for handling large number of input values

    This object does not actually store the input values, but will instead keep a running tally of the various
    statistics. This does prevent working back through previous numbers and undo-ing some steps, but provides
    better support for large sets of numbers (>1,000,000) with very little memory overhead and computations.

    Better Performance!!!
    """
    def __init__(self):
        self._min = None
        self._max = None
        self._updated_values = collections.deque()
        self._running_average = 0.
        self._input_count = 0

    def __str__(self):
        """
        Simplifies string representations of the objects

        Returns:
            str
        """
        return "{0} objects, min: {1}, max: {2}, mean: {3:{4}}".format(
            self.get_count(), self.get_min(), self.get_max(), self.get_mean(), "" if self.get_mean() is None else "3.5f"
        )

    def get_count(self):
        """
        Provides the total number of input values inserted into the Tracker

        Returns:
            int: Number of inserted values
        """
        return self._input_count + len(self._updated_values)

    def insert(self, new_temp):
        """
        Adds a new value to the running statistics aggregators

        Where the magic happens. Updates the min/max values but also updates a running average using the formula:
        :math:`x' = x(n / (n + 1)) + a / (n + 1)`
        where:
        - :math:`x'` = New running average
        - :math:`x` = Previous average
        - :math:`n` = Number of values used in the previous average
        - :math:`a` = New input value for the running average

        The formula was modified slightly to create a ratio before multiplying to the previous average to keep the
        resulting value smaller than if the previous average was multiplied by the previous count.

        Args:
            new_temp (int): New input value to add to the running statistics.
        """
        # Test if a new Min is found
        if self._min is None or new_temp < self._min:
            self._min = new_temp

        # Test if a new Max is found
        if self._max is None or new_temp > self._max:
            self._max = new_temp

        # Create cache for new values
        self._updated_values.append(new_temp)

        # If cache is getting large, process it and prepare for new values
        if len(self._updated_values) > 100000:
            self.get_mean()

    def get_min(self):
        """
        Provide the minimum value

        Notes:
            'None' is returned if no value has been inserted yet.

        Returns:
            int: Minimum of all inserted values
        """
        return self._min

    def get_max(self):
        """
        Provide the maximum value

        Notes:
            'None' is returned if no value has been inserted yet.

        Returns:
            int: Maximum of all inserted values
        """
        return self._max

    def get_mean(self):
        """
        Calculates new Mean and provides the mean value

        Notes:
            'None' is returned if not values are inserted yet.

        Returns:
            int: Mean of all inserted values
        """
        if len(self._updated_values) > 0:
            # Update the running average
            new_sum = 0
            new_count = 0
            while len(self._updated_values) > 0:
                new_sum += self._updated_values.pop()
                new_count += 1

            self._running_average = \
                self._running_average * (self._input_count / float(self._input_count + new_count)) + \
                new_sum / float(self._input_count + new_count)

            # Increase the count
            self._input_count += new_count
            self._updated_values.clear()
        return self._running_average if self._input_count > 0 else None
