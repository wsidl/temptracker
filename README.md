# TempTracker: Temperature Statistics Aggregator
[//]: # (Will Siddall <will.siddall@gmail.com>)
[//]: # (0.1, 29-05-2018: First Draft)

This project outlines the use of a Python object for aggregating statistics of temperature inputs
and providing real-time updates.

## Installation
TempTracker can be installed in a number of ways. Choose the options that best describes the appropriate method
for your situation.

### Wheel File
This method allows for a seamless integration into your Python environment by using the built-in tools to install and
also provides the necessary metadata for the project.


First download the `.whl` file to the local environment and from a command prompt or terminal, type the following command.

```bash
> pip install temptracker-0.0.1-any.whl
```

or on Windows:

```batch
C:\> C:\Pythonxx\Scripts\pip install temptracker-0.0.1-any.whl
```

### Portable Install
Maybe it is necessary to embed the class into a larger project. In this case, it may be desired to only copy the
Python file directly into the project.

This can either be done by:
* Downloading the Python file directly from the Git Repository.
* Cloning the Git Repository locally and copying the file to a new project.
* Extracting the `.whl` file using a Zip Utility and copying the file.

As long as you can view the `.py` file, the file can be moved into a new location.

## Usage
The object is designed to be small and lightweight. This does mean it lacks in some more advanced functionality but also
means it provides a focus on a small subset of features while providing all of the performance needed for it's task.

Once installed, the library can be imported and initialized
```python
> import temp_tracker
> new_tracker = temp_tracker.TempTracker()
```

Once initialized, the object can accept inputs and provide updated statistics.

```python
>>> new_tracker.insert(87)
>>> new_tracker.get_min()
87
>>> new_tracker.get_mean()
87.0
```

As more values are added, the mean keeps updating regardless of how many input values are added.
```python
>>> import random
>>> for a in range(99999)
...     new_tracker.insert(random.randint(0, 110))
>>> new_tracker.get_count()
99999
>>> new_tracker.get_mean()
54.80441804417957
```
