import setuptools

with open("README.md", 'r') as ld:
    long_desc = ld.read()

setuptools.setup(
    name="TempTracker",
    version="0.1",
    description="Temperature Tracker",
    long_description=ld,
    long_description_content_type="text/markdown"
)
