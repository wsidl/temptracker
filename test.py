import unittest
import temp_tracker


class TestTempTracker(unittest.TestCase):
    def setUp(self):
        self.tt = temp_tracker.TempTracker()
        for a in [29, 101, 18, 72]:
            self.tt.insert(a)

    def test_min_value(self):
        self.assertEqual(18, self.tt.get_min(), "Tracker did not return the correct minimum")

    def test_max_value(self):
        self.assertEqual(101, self.tt.get_max(), "Tracker did not return the correct maximum")

    def test_mean_value(self):
        self.assertEqual(55.0, self.tt.get_mean(), "Tracker did not return the correct mean")


class TestLargeTempTracker(unittest.TestCase):
    def setUp(self):
        self.tt = temp_tracker.TempTracker()
        for a in range(0, 1000001):
            self.tt.insert(a)

    def test_min_value(self):
        self.assertEqual(0, self.tt.get_min(), "Tracker did not return the correct minimum")

    def test_max_value(self):
        self.assertEqual(1000000, self.tt.get_max(), "Tracker did not return the correct maximum")

    def test_mean_value(self):
        self.assertAlmostEqual(500000.0, self.tt.get_mean(), 5, "Tracker did not return the correct mean")


if __name__ == '__main__':
    unittest.main()
